import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TripReportPage } from './trip-report';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';
import { OnCreate } from './dummy-directive';

@NgModule({
  declarations: [
    TripReportPage,
    OnCreate
  ],
  imports: [
    IonicPageModule.forChild(TripReportPage),
    SelectSearchableModule,
    TranslateModule.forChild()
  ],
  exports: [
    OnCreate
  ],
})
export class TripReportPageModule {}
