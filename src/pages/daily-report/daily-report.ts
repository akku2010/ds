import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Platform } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
import { SocialSharing } from '@ionic-native/social-sharing';
import { File } from '@ionic-native/file';
import * as XLSX from 'xlsx';
import * as jsPDF from 'jspdf';
// import domtoimage from 'dom-to-image';
// import { FileOpener } from '@ionic-native/file-opener';
import html2canvas from 'html2canvas';

@IonicPage()
@Component({
  selector: 'page-daily-report',
  templateUrl: 'daily-report.html',
})
export class DailyReportPage implements OnInit {

  deviceReport: any[] = [];
  deviceReportSearch: any = [];
  selectedVehicle: any;
  to: string;
  from: string;
  islogin: any;
  todaydate: Date;
  todaytime: string;
  datetime: number;

  page: number = 0;
  limit: number = 10;
  fromDate: Date;
  vehicleData: any = [];
  dataTablesParameters: any;
  portstemp: any[] = [];

  loading: any;
  twoMonthsLater: any = moment().subtract(2, 'month').format("YYYY-MM-DD");
  today: any = moment().format("YYYY-MM-DD");
  pltStr: string;

  constructor(
    public file: File,
    public socialSharing: SocialSharing,
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicalldaily: ApiServiceProvider,
    private alertCtrl: AlertController,
    // private fileOpener: FileOpener,
    private loadingCtrl: LoadingController,
    private plt: Platform
  ) {

    if (this.plt.is('android')) {
      this.pltStr = 'md';
    } else if (this.plt.is('ios')) {
      this.pltStr = 'ios';
    }

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};

    this.from = moment({ hours: 0 }).format();
    console.log('start date', this.from)
    this.to = moment().format();//new Date(a).toISOString();
    console.log('stop date', this.to);
    ////////////////

    // var today = moment().format("YYYY");
    // var oneMonthLater = moment().subtract(1, 'month').format("DD MMMM YYYY");
    // var abc = moment().subtract(2, 'month').format("DD MMMM YYYY");
    // this.twoMonthsLater = moment().subtract(2, 'month').format("YYYY-MM-DD");

    // console.log("Today:", today);
    // console.log("Month ago:", oneMonthLater);
    // console.log("Two Months ago:", this.twoMonthsLater);
    /////////////////
    if (navParams.get('param') != null) {
      // this.vehicleData = navParams.get('param');
      this.vehicleData.push(navParams.get('param').Device_ID);
    }
  }

  ngOnInit() {
    this.getdevices();
    this.getDailyReportData();
  }

  ionViewDidEnter() {
    this.getDefaultUserSettings();
  }

  measurementUnit: string = 'MKS';
  getDefaultUserSettings() {
    var b_url = this.apicalldaily.mainUrl + "users/get_user_setting";
    var Var = { uid: this.islogin._id };
    this.apicalldaily.urlpasseswithdata(b_url, Var)
      .subscribe(resp => {
        console.log("check lang key: ", resp)
        if (resp.unit_measurement !== undefined) {
          this.measurementUnit = resp.unit_measurement;
        } else {
          if (localStorage.getItem('MeasurementType') !== null) {
            let measureType = localStorage.getItem('MeasurementType');
            this.measurementUnit = measureType;
          } else {
            this.measurementUnit = 'MKS';
          }
        }
      },
        err => {
          console.log(err);
          if (localStorage.getItem('MeasurementType') !== null) {
            let measureType = localStorage.getItem('MeasurementType');
            this.measurementUnit = measureType;
          } else {
            this.measurementUnit = 'MKS';
          }
        });
  }

  getItems(ev: any) {
    const val = ev.target.value.trim();
    this.deviceReportSearch = this.deviceReport.filter((item) => {
      return (item.device.Device_Name.toLowerCase().indexOf(val.toLowerCase()) > -1);
    });
  }

  getDailyReportData() {
    console.log("entered");
    this.page = 0;
    var baseUrl;
    baseUrl = this.apicalldaily.mainUrl + "devices/daily_report";
    let that = this;
    var currDay = new Date().getDay();
    var currMonth = new Date().getMonth();
    var currYear = new Date().getFullYear();
    var selectedDay = new Date(that.to).getDay();
    var selectedMonth = new Date(that.to).getMonth();
    var selectedYear = new Date(that.to).getFullYear();
    var devname, devid, today_odo, today_running, today_stopped, t_idling, t_ofr, today_trips, maxSpeed, mileage;
    if ((currDay == selectedDay) && (currMonth == selectedMonth) && (currYear == selectedYear)) {
      devname = "Device_Name";
      devid = "Device_ID";
      today_odo = "today_odo";
      today_running = "today_running";
      today_stopped = "today_stopped";
      t_idling = "t_idling"
      t_ofr = "t_ofr";
      today_trips = "today_trips";
      maxSpeed = "maxSpeed";
      mileage = "Mileage"
    } else {
      console.log("else block called");
      devid = "imei";
      devname = "ID.Device_Name";
      today_odo = "today_odo";
      today_running = "today_running";
      today_stopped = "today_stopped";
      t_idling = "t_idling"
      t_ofr = "t_ofr";
      today_trips = "today_trips";
      maxSpeed = "ID.maxSpeed";
      mileage = "Mileage"
    }
    // debugger
    var payload = {};
    if (this.vehicleData.length === 0) {
      payload = {
        "draw": 2,
        "columns": [
          {
            "data": devname
          },
          {
            "data": devid
          },
          {
            "data": today_odo
          },
          {
            "data": today_running
          },
          {
            "data": today_stopped
          },
          {
            "data": t_idling
          },
          {
            "data": t_ofr
          },
          {
            "data": today_trips
          },
          {
            "data": maxSpeed
          },
          {
            "data": mileage
          },
          { "data": "t_running" },
          { "data": "t_stopped" },
          { "data": "t_idling" },
          { "data": "t_ofr" },
          { "data": "t_noGps" },
          {
            "data": null,
            "defaultContent": ""
          }
        ],
        "order": [
          {
            "column": 0,
            "dir": "asc"
          }
        ],
        "start": 0,
        "length": this.limit,
        "search": {
          "value": "",
          "regex": false
        },
        "op": {},
        "select": [],
        "find": {
          "user_id": this.islogin._id,
          "date": new Date(this.to).toISOString()
        }
      }
    } else {
      payload = {
        "draw": 2,
        "columns": [
          {
            "data": devname
          },
          {
            "data": devid
          },
          {
            "data": today_odo
          },
          {
            "data": today_running
          },
          {
            "data": today_stopped
          },
          {
            "data": t_idling
          },
          {
            "data": t_ofr
          },
          {
            "data": today_trips
          },
          {
            "data": mileage
          },
          {
            "data": maxSpeed
          },
          { "data": "t_running" },
          { "data": "t_stopped" },
          { "data": "t_idling" },
          { "data": "t_ofr" },
          { "data": "t_noGps" },
          {
            "data": null,
            "defaultContent": ""
          }
        ],
        "order": [
          {
            "column": 0,
            "dir": "asc"
          }
        ],
        "start": 0,
        "length": this.limit,
        "search": {
          "value": "",
          "regex": false
        },
        "op": {},
        "select": [],
        "find": {
          "user_id": this.islogin._id,
          // "devId": this.vehicleData.Device_ID,
          "devId": this.vehicleData,
          "date": new Date(this.to).toISOString()
        }
      }
    }
    this.deviceReport = [];
    this.apicalldaily.startLoading().present();
    this.apicalldaily.getDailyReport1(baseUrl, payload)
      .subscribe(data => {
        this.apicalldaily.stopLoading();
        console.log("daily report data: ", data)
        for (var i = 0; i < data.data.length; i++) {

          // var ignOff = 86400000 - parseInt(data.data[i].today_running);
          // var ign_off = that.millisecondConversion(ignOff);
          // debugger
          this.deviceReport.push({
            _id: data.data[i]._id,
            Device_ID: data.data[i].Device_ID ? data.data[i].Device_ID : data.data[i].imei,
            Device_Name: data.data[i].Device_Name ? data.data[i].Device_Name : (data.data[i].ID ? data.data[i].ID.Device_Name : 'N/A'),
            maxSpeed: data.data[i].maxSpeed ? data.data[i].maxSpeed : (data.data[i].ID ? data.data[i].ID.maxSpeed : '0'),
            today_odo: data.data[i].today_odo,
            today_running: this.millisToMinutesAndSeconds(data.data[i].today_running),
            // today_stopped: this.millisToMinutesAndSeconds(ignOff),
            today_stopped: this.millisToMinutesAndSeconds(data.data[i].today_stopped),
            t_idling: this.millisToMinutesAndSeconds(data.data[i].t_idling),
            t_ofr: this.millisToMinutesAndSeconds(data.data[i].t_ofr),
            today_trips: data.data[i].today_trips,
            mileage: data.data[i].Mileage ? ((data.data[i].today_odo) / Number(data.data[i].Mileage)).toFixed(2) : 'N/A'
            // avgSpeed: this.calcAvgSpeed(odo1[0], data.data[i].today_running)
          })
        }

      }, error => {
        this.apicalldaily.stopLoading();
        console.log("error in service=> " + error);
      })
  }

  getdevices() {
    var baseURLp = this.apicalldaily.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }

    this.apicalldaily.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        // this.apicalldaily.stopLoading();
        this.portstemp = data.devices;
      },
        err => {
          // this.apicalldaily.stopLoading();
          console.log(err);
        });
  }

  getSelectedId(selectedVehicle) {
    // console.log(pdata)
    // this.vehicleData = pdata;
    // debugger
    this.vehicleData = [];
    if (selectedVehicle.length > 0) {
      if (selectedVehicle.length > 1) {
        for (var t = 0; t < selectedVehicle.length; t++) {
          this.vehicleData.push(selectedVehicle[t].Device_ID);
        }
        this.getDailyReportData();
      } else {
        this.vehicleData.push(selectedVehicle[0].Device_ID);
        this.getDailyReportData();
      }
    } else return;
    console.log("selectedVehicle=> ", this.vehicleData)
    // this.getDailyReportData();
  }

  millisToMinutesAndSeconds(millis) {
    var ms = millis;
    ms = 1000 * Math.round(ms / 1000); // round to nearest second
    var d = new Date(ms);
    // debugger
    var min1;
    var min = d.getUTCMinutes();
    if ((min).toString().length == 1) {
      min1 = '0' + (d.getUTCMinutes()).toString();
    } else {
      min1 = min;
    }

    return d.getUTCHours() + ':' + min1;
  }

  OnClick() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Select Option');

    alert.addInput({
      type: 'radio',
      label: 'Export to PDF',
      value: 'pdf',
      checked: true
    });
    alert.addInput({
      type: 'radio',
      label: 'Export to Excel',
      value: 'excel'
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        console.log("check alert val: ", data)
        if (data === 'pdf') {
          this.OnExportPDF();
        } else {
          this.OnExport();
        }
      }
    });
    alert.present();
  }

  presentLoading(msg) {
    this.loading = this.loadingCtrl.create({
      content: msg
    });
    return this.loading.present();
  }

  OnExportPDF() {
    debugger
    this.presentLoading('Creating PDF file...');
    const div = document.getElementById("printable-area");

    let self = this;

    html2canvas(div).then((canvas) => {
      canvas.getContext('2d');
      var image = canvas.toDataURL("image/png");

      //Initialize JSPDF
      // var doc = new jsPDF();
      var doc = new jsPDF('p', 'pt', 'a4');
      var width = doc.internal.pageSize.width;
      var height = doc.internal.pageSize.height;
      var options = {
        pagesplit: true
      };
      doc.text(10, 20, 'Daily Report');
      var h1 = 50;
      var aspectwidth1 = (height - h1) * (9 / 16);
      //Add image Url to PDF
      doc.addImage(image, 'JPEG', 10, -20, aspectwidth1, (height - h1), 'monkey');

      let pdfOutput = doc.output();

      // ArrayBuffer will allow you to put image inside PDF
      let buffer = new ArrayBuffer(pdfOutput.length);
      let array = new Uint8Array(buffer);
      for (var i = 0; i < pdfOutput.length; i++) {
        array[i] = pdfOutput.charCodeAt(i);
      }
      //PDF file will stored , you can change this line as you like

      const directory = self.file.dataDirectory;
      const fileName = "daily_report_download.pdf";

      self.file.checkFile(directory, fileName).then((success) => {
        //Writing File to Device
        self.file.writeFile(directory, fileName, buffer, { replace: true })
          .then((success) => {
            self.loading.dismiss();
            console.log("File created Succesfully" + JSON.stringify(success));
            self.socialSharing.share('PDF file export', 'PDF export', success['nativeURL'], '')

          })
          .catch((error) => {
            self.loading.dismiss();
            console.log("Cannot Create File " + JSON.stringify(error));
          });
      })
        .catch((error) => {

          //Writing File to Device
          self.file.writeFile(self.file.dataDirectory, "daily_report_download.pdf", buffer, { replace: true })
            // self.file.writeFile(directory, fileName, buffer, { replace: true })
            .then((success) => {
              self.loading.dismiss();
              alert("File created Succesfully" + JSON.stringify(success));
              ///////////
              self.socialSharing.share('PDF file export', 'PDF export', success['nativeURL'], '')
              ///////////
            })
            .catch((error) => {
              self.loading.dismiss();
              console.log("Cannot Create File " + JSON.stringify(error));
            });
        });

    })
      .catch((error) => {
        self.loading.dismiss();
        console.error('oops, something went wrong!', error);
      });

  }

  OnExport = function () {
    let sheet = XLSX.utils.json_to_sheet(this.deviceReport);
    let wb = {
      SheetNames: ["export"],
      Sheets: {
        "export": sheet
      }
    };

    let wbout = XLSX.write(wb, {
      bookType: 'xlsx',
      bookSST: false,
      type: 'binary'
    });

    function s2ab(s) {
      let buf = new ArrayBuffer(s.length);
      let view = new Uint8Array(buf);
      for (let i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    }

    let blob = new Blob([s2ab(wbout)], { type: 'application/octet-stream' });
    let self = this;

    // this.getStoragePath().then(function (url) {
    self.file.writeFile(self.file.dataDirectory, "daily_report_download.xlsx", blob, { replace: true })
      .then((stuff) => {
        // alert("file downloaded at: " + self.file.dataDirectory);
        if (stuff != null) {
          // self.socialSharing.share('CSV file export', 'CSV export', url, '')
          self.socialSharing.share('CSV file export', 'CSV export', stuff['nativeURL'], '')
        } else return Promise.reject('write file')
      }).catch(() => {
        alert("error creating file at :" + self.file.dataDirectory);
      });
    // });
  }

  calcAvgSpeed(distance: any, time: any) {
    distance = distance / 1000;      //1000 = km
    time = time / 3600000;           // 1000 = sec, 60000 = min, 3600000 = hrs
    return distance * 3600000 / time;
  }

  doInfinite(infiniteScroll) {
    let that = this;
    that.page = that.page + 10;
    setTimeout(() => {
      var baseUrl, payload = {};
      baseUrl = this.apicalldaily.mainUrl + "devices/daily_report";
      let that = this;
      var currDay = new Date().getDay();
      var currMonth = new Date().getMonth();
      var currYear = new Date().getFullYear();
      var selectedDay = new Date(that.to).getDay();
      var selectedMonth = new Date(that.to).getMonth();
      var selectedYear = new Date(that.to).getFullYear();
      var devname, devid, today_odo, today_running, today_stopped, t_idling, t_ofr, today_trips, maxSpeed, mileage;

      if ((currDay == selectedDay) && (currMonth == selectedMonth) && (currYear == selectedYear)) {
        devname = "Device_Name";
        devid = "Device_ID";
        today_odo = "today_odo";
        today_running = "today_running";
        today_stopped = "today_stopped";
        t_idling = "t_idling";
        t_ofr = "t_ofr;"
        today_trips = "today_trips";
        maxSpeed = "maxSpeed";
        mileage = "Mileage";
      } else {
        console.log("else block called");
        devid = "imei";
        devname = "ID.Device_Name";
        today_odo = "today_odo";
        today_running = "today_running";
        today_stopped = "today_stopped";
        t_idling = "t_idling";
        t_ofr = "t_ofr";
        today_trips = "today_trips";
        maxSpeed = "ID.maxSpeed";
        mileage = "Mileage";
      }
      var payload = {};
      if (that.vehicleData == undefined) {
        payload = {
          "draw": 2,
          "columns": [
            {
              "data": devname
            },
            {
              "data": devid
            },
            {
              "data": today_odo
            },
            {
              "data": today_running
            },
            {
              "data": today_stopped
            },
            {
              "data": t_idling
            },
            {
              "data": t_ofr
            },
            {
              "data": today_trips
            },
            {
              "data": mileage
            },
            {
              "data": maxSpeed
            },
            { "data": "t_running" },
            { "data": "t_stopped" },
            { "data": "t_idling" },
            { "data": "t_ofr" },
            { "data": "t_noGps" },
            {
              "data": null,
              "defaultContent": ""
            }
          ],
          "order": [
            {
              "column": 0,
              "dir": "asc"
            }
          ],
          "start": that.page,
          "length": 10,
          "search": {
            "value": "",
            "regex": false
          },
          "op": {},
          "select": [],
          "find": {
            "user_id": this.islogin._id,
            "date": new Date(this.to).toISOString()
          }
        }
      } else {
        payload = {
          "draw": 2,
          "columns": [
            {
              "data": devname
            },
            {
              "data": devid
            },
            {
              "data": today_odo
            },
            {
              "data": today_running
            },
            {
              "data": today_stopped
            },
            {
              "data": t_idling
            },
            {
              "data": t_ofr
            },
            {
              "data": today_trips
            },
            {
              "data": maxSpeed
            },
            {
              "data": mileage
            },
            { "data": "t_running" },
            { "data": "t_stopped" },
            { "data": "t_idling" },
            { "data": "t_ofr" },
            { "data": "t_noGps" },
            {
              "data": null,
              "defaultContent": ""
            }
          ],
          "order": [
            {
              "column": 0,
              "dir": "asc"
            }
          ],
          "start": that.page,
          "length": 10,
          "search": {
            "value": "",
            "regex": false
          },
          "op": {},
          "select": [],
          "find": {
            "user_id": this.islogin._id,
            "devId": this.vehicleData.Device_ID,
            "date": new Date(this.to).toISOString()
          }
        }
      }

      this.apicalldaily.getDailyReport1(baseUrl, payload)
        .subscribe(data => {
          console.log("daily report data: ", data)
          for (var i = 0; i < data.data.length; i++) {
            that.deviceReport.push({
              _id: data.data[i]._id,
              Device_ID: data.data[i].Device_ID ? data.data[i].Device_ID : data.data[i].imei,
              Device_Name: data.data[i].Device_Name ? data.data[i].Device_Name : (data.data[i].ID ? data.data[i].ID.Device_Name : 'N/A'),
              maxSpeed: data.data[i].maxSpeed ? data.data[i].maxSpeed : (data.data[i].ID ? data.data[i].ID.maxSpeed : '0'),
              today_odo: data.data[i].today_odo,
              today_running: this.millisToMinutesAndSeconds(data.data[i].today_running),
              today_stopped: this.millisToMinutesAndSeconds(data.data[i].today_stopped),
              t_idling: this.millisToMinutesAndSeconds(data.data[i].t_idling),
              t_ofr: this.millisToMinutesAndSeconds(data.data[i].t_ofr),
              today_trips: data.data[i].today_trips,
              avgSpeed: this.calcAvgSpeed(data.data[i].today_odo, data.data[i].today_running),
              mileage: data.data[i].Mileage ? ((data.data[i].today_odo) / Number(data.data[i].Mileage)).toFixed(2) : 'N/A'
            })
          }
          console.log('Async operation has ended');
          infiniteScroll.complete();
        })


    }, 200);
  }
}
